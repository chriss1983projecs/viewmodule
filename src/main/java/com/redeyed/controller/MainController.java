package com.redeyed.controller;

import javafx.beans.property.BooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MainController implements FxmlController {

//	private final HibernateConfigUtil configUtil;

	@FXML
	private MenuItem		morePlayersMenuItem;
	@FXML
	private MenuItem		moreTeamsMenuItem;
	@FXML
	private CheckMenuItem	showFieldGoalPercentage;
	

	@Autowired
	public MainController() {
//		if(redeyedCoreManager == null) {
//			System.out.println("errror");
//		}
//		this.configUtil = configUtil;
	}

	public void initialize() {
		// No extra initialization required
	}
	

	@FXML
	public void loadMorePlayers() {
		morePlayersMenuItem.setDisable(true);
//		model.fetchRemainingPlayers();
	}

	@FXML
	public void loadMoreTeams() {
		moreTeamsMenuItem.setDisable(true);
//		model.fetchRemainingTeams();
	}

	public BooleanProperty showFieldGoalPercentageProperty() {
		return showFieldGoalPercentage.selectedProperty();
	}

}
