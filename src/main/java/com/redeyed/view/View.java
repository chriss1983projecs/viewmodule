package com.redeyed.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;


public class View extends Application {
	private Parent root;

    public static void main(final String[] args) {
        Application.launch(args);
    }

	@Override
	public void init() throws Exception {

//		log.debug("init()");

		ConfigurableApplicationContext context = startSpringApplication();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
//		Test t = new Test();
		loader.setControllerFactory(clazz -> context.getBean(clazz));
		root = loader.load();
	}

	@Override
	public void start(Stage stage) throws Exception {
		// Close the Spring context when the client is closed.
//		log.debug("start");
		stage.setOnCloseRequest(e -> System.exit(0));
		stage.setScene(new Scene(root, 850, 550));
		stage.setTitle("Spring Client Application");
		stage.show();
	}

	private ConfigurableApplicationContext startSpringApplication() {
//		log.debug("startSpringApplication()");
		SpringApplication application = new SpringApplication(View.class);
		String[] args = getParameters().getRaw().stream().toArray(String[]::new);
		application.setHeadless(false);
		return application.run(args);
	}

}
